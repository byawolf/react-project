const express = require('express');
const router = express.Router();
import fs from 'fs';
import path from 'path';

router.get('/video/:lecture', function(req, res) {
  const fullpath = path.resolve('wwwroot', '../public/mp4/', './' + req.params.lecture);

  if (!fullpath || fullpath.length < 0) {
      return res.status(400).send({error: 'invalid path.'});
  }

  const stat = fs.statSync(fullpath);

  const fileSize = stat.size;

  const range = req.headers.range;
  if(range) {
      const parts = range.replace(/bytes=/, "").split("-");
      const start = parseInt(parts[0], 10);
      const end = parts[1]
          ? parseInt(parts[1], 10)
          : fileSize-1;
      const chunksize = (end-start)+1;
      const file = fs.createReadStream(fullpath, {start, end});
      const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': 'video/mp4',
      };

      res.writeHead(206, head);
      file.pipe(res);
  } else {
      const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
      };
      res.writeHead(200, head);
      fs.createReadStream(fullpath).pipe(res);
  }
});

module.exports = router;