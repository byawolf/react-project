const express = require('express');
const router = express.Router();
const controller = require('../controllers/lecture.js');
import passport from 'passport';

const requireAuth = passport.authenticate('jwt', { session: false });


router.get('/lecture/:id', controller.search);
router.post('/add-chapter', requireAuth, controller.addChapter);
router.post('/add-lecture', requireAuth, controller.addLecture);
router.post('/view-lecture', requireAuth, controller.viewById);

module.exports = router;