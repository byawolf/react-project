const express = require('express');
const router = express.Router();
const controller = require('../controllers/course.js');
import passport from 'passport';

const requireAuth = passport.authenticate('jwt', { session: false });

router.get('/paginate', controller.paginate);
router.get('/courses/:keyword', controller.search);
router.get('/courses/detail/:id', controller.searchById);
router.get('/view-courses', requireAuth, controller.buyedCourses);
router.post('/add-course', requireAuth, controller.add);
router.post('/update-course', requireAuth, controller.update);
router.post('/remove-course', requireAuth, controller.remove);

module.exports = router;