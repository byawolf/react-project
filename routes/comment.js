const express = require('express');
const router = express.Router();
const controller = require('../controllers/comment');
import passport from 'passport';

const requireAuth = passport.authenticate('jwt', { session: false });

router.get('/comments', controller.paginate);
router.post('/add-comment', requireAuth, controller.addComment);
router.post('/remove-comment', requireAuth, controller.removeComment);

module.exports = router;