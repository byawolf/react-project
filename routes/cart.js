const express = require('express');
const router = express.Router();
const controller = require('../controllers/cart.js');
import passport from 'passport';

const requireAuth = passport.authenticate('jwt', { session: false });

router.post('/buy-cart', requireAuth, controller.buyCart);
router.get('/list-cart', requireAuth, controller.listCart);
router.post('/add-cart', requireAuth, controller.addCart);
router.post('/remove-cart', requireAuth, controller.removeCart);

module.exports = router;