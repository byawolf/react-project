import passportLogin from '../services/passport';
passportLogin();

const authRoutes = require("./auth.js");
const cartRoutes = require("./cart.js");
const commentRoutes = require("./comment.js");
const courseRoutes = require("./course.js");
const lectureRoutes = require("./lecture.js");
const videoRoutes = require("./video.js");

export default (app) => {
  app.use("", authRoutes);
  app.use("", cartRoutes);
  app.use("", commentRoutes);
  app.use("", courseRoutes);
  app.use("", lectureRoutes);
  app.use("", videoRoutes);
};
