const express = require('express');
const router = express.Router();
const controller = require('../controllers/authentication.js');
import passport from 'passport';


const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });


router.get('/', requireAuth, function(req, res) {
  res.send({ message: 'Welcome! guest: (' + req.user.email +')'});
});
router.get('/token', requireAuth, controller.userinfo);
router.post('/signin', requireSignin, controller.signin);
router.get('/signout', requireAuth, controller.signout);
router.post('/signup', controller.signup);

module.exports = router;