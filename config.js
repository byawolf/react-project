module.exports = {
    hostUrl: 'http://localhost:5000',
    clientUrl: 'http://localhost:3000',
    database: 'mongodb://localhost:27017/e-learning',
    secret: "secret_diploma",
    port: 5000
};
