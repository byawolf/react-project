import jwt from 'jsonwebtoken';
import config from '../config';

const generateToken = (user) => {
    const timestamp = new Date().getTime();
    const role = user.profile.instructor ? "instructor" : "user";
    const token = jwt.sign({sub: user.id, iat: timestamp, role: role}, config.secret);

    return token;
};

export default function (user) {
    return generateToken(user);
}
