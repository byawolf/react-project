import _ from 'lodash';
import Lecture from '../models/lecture';
import config, { hostUrl } from '../config';
import path from 'path';

export const addChapter = async function (req, res, next) {
    const { lectureNo, title } = req.body;
    try {
        const lecture = await Lecture.findOne({ no: lectureNo });
        await Lecture.update({
            'no': lectureNo
        }, {
            $push: {
                header: {
                    body: [],
                    title: title,
                    no: lecture.header.length
                }
            }
        });
      
        return res.json({
            lectureNo: lectureNo,
            chapterNo: lecture.header.length
        }).end();
    } catch (err) { 
        return next(err);
    };
};

export const addLecture = async function (req, res, next) {
    const { lectureNo, chapterNo, content } = req.body;
    try {
        const lecture = await Lecture.findOne({ no: lectureNo })
        let filename = `${hostUrl}/video/default.mp4`;
        const video = req.files ? req.files.video : null;
        if(video) {
            const name = video.name.replace('.', Date.now() + '.');
            const fullpath = path.resolve('wwwroot', '../public/mp4/', './' + name);
    
            await video.mv(fullpath, function (err) {
                if (err) {
                    return next(err);
                }
            });
    
            if(name && name.length > 0) {
                filename = `${hostUrl}/video/${name}`;
            }
        }

        const newLectur = {
            sub_no: lecture.header[chapterNo].body.length,
            content: content,
            video: filename
        };
        lecture.header[chapterNo].body.push(newLectur);  

        await Lecture.findByIdAndUpdate(lecture._id, lecture);
        return res.json({
            lecture: 'added',
        }).end();
    } catch (err) { 
        return next(err);
    };
};

export const search = async function (req, res, next) {
    const id = req.params.id;

    if (!id || id.length < 0) {
        return res.status(400).send({error: 'invalid id.'});
    }

    Lecture.find({no: id})
        .populate({
            path: '_course'
        })
        .exec(function (error, doc) {
            if (error) {
                return next(error);
            }

            if (doc) {
                return res.json(doc[0]);
            }
        });
};

export const viewById = async function (req, res, next) {
    const lecture_no = req.body.lecture_no;
    const header_no = req.body.header_no;
    const sub_no = req.body.sub_no;

    try { 
        const doc = await Lecture.find({ no: lecture_no })
            .populate({
                path: '_course'
            })
            .exec();
        if (!doc) return;

        const header_filtered = doc[0].header.filter(elem => elem.no === header_no);
        
        if (!header_filtered || !header_filtered.length) return;
          
        const body_filtered = header_filtered[0].body.filter(elem => elem.sub_no === sub_no);
        if (!body_filtered || !body_filtered.length) return;

        return res.json(body_filtered[0]);

    } catch (err) { 
        return next(err);
    };
};
