import _ from 'lodash';


export const search = async function (req, res, next) {
    const email = req.user.email;

    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    try {
        const user = await User.findOne({ email: email })
            .populate({
                path: 'courses.ref',
                populate: {
                    path: '_authors',
                    model: 'User'
                }
            })
            .exec();
        if (!user) return;

        const filtered = user.courses.filter(elem => elem.learn === true);

        if (!filtered.length) {
            return res.json([]);
        }
        
        const results = filtered.map(elem => elem.ref);
        return res.json(results);
    } catch (err) {
        console.log(err);
    };
};
