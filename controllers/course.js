import _ from 'lodash';
const rand = require('random-seed').create();
import config, { hostUrl } from '../config';
import path from 'path';
import User from '../models/user';
import Course from '../models/course';
import getNo from './getNo';
import Lecture from '../models/lecture';


export const search = async function (req, res, next) {
    const keyword = req.params.keyword;

    if (!keyword || keyword.length < 0) {
        return res.status(400).send({error: 'invalid keyword.'});
    }

    const regex = new RegExp('^.*' + keyword + '.*$', 'i');

    Course
        .find()
        .populate({
            path: '_authors'
        })
        .or([
            {'title': {$regex: regex}},
            {'authors': {$elemMatch: {'name': {$regex:regex}}}}
            ])
        .sort({updated: -1})
        .exec(function(err, results) {
            if (err) {
                return next(err);
            }

            if (results) {
                return res.json({"courses": results});
            }
        });
};

export const searchById = async function (req, res, next) {
    const id = req.params.id;

    if (!id || id.length < 0) {
        return res.status(400).send({error: 'invalid id.'});
    }

    Course.find({no: id})
        .populate({
            path: '_authors'
        })
        .exec(function(error, doc) {
            if (error) {
                return next(error);
            }

            if (doc) {
                return res.json(doc[0]);
            }
    });
};

export const paginate = async function (req, res, next) {
    const keyword = req.query.keyword;

    if (!keyword || keyword.length <= 0) {
        return paginateAll(req, res, next);
    }
    else {
        return paginateQuery(req, res, next);
    }
};

const paginateAll = async function (req, res, next) {
    const page = parseInt(req.query.page);
    if (!page || page <= 0) {
        return res.status(400).send({error: 'invalid page.'});
    }

    const limit = parseInt(req.query.limit);
    if (!limit || limit <= 0) {
        return res.status(400).send({error: 'invalid limit.'});
    }

    const sort_type = req.query.sort_type;
    if(!sort_type) {
        return res.status(400).send({error: 'invalid sort type.'});
    }

    const sort_order = parseInt(req.query.sort_order);
    if(!sort_order) {
        return res.status(400).send({error: 'invalid sort order.'});
    }

    try {
        const count = await Course.count().exec();
        const total = Math.ceil(count / limit);

        const results = await Course
            .find()
            .populate({ path: '_authors' })
            .sort({ [sort_type]: sort_order })
            .skip((page - 1) * limit)
            .limit(limit)
            .exec();
        if (results) {
            return res.json({
                'page': page,
                'limit': limit,
                'total': total,
                'courses': results
            });
        }
    } catch (err) {
        return next(err);  
    };
};

const paginateQuery = async function (req, res, next) {
    const keyword = req.query.keyword;
    if (!keyword || keyword.length < 0) {
        return res.status(400).send({error: 'invalid keyword.'});
    }

    const page = parseInt(req.query.page);
    if (!page || page <= 0) {
        return res.status(400).send({error: 'invalid page.'});
    }

    const limit = parseInt(req.query.limit);
    if (!limit || limit <= 0) {
        return res.status(400).send({error: 'invalid limit.'});
    }

    const sort_type = req.query.sort_type;
    if(!sort_type) {
        return res.status(400).send({error: 'invalid sort type.'});
    }

    const sort_order = parseInt(req.query.sort_order);
    if(!sort_order) {
        return res.status(400).send({error: 'invalid sort order.'});
    }

    const _escaped = keyword.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
    const regex = new RegExp('^.*' + _escaped + '.*$', 'i');

    try {
        const count = await Course
            .find()
            .populate({ path: '_authors' })
            .or([
                { 'title': { $regex: regex } },
                { 'authors': { $elemMatch: { 'name': { $regex: regex } } } }
            ])
            .sort({ [sort_type]: sort_order })
            .count()
            .exec();
        
        const total = Math.ceil(count / limit);
        const results = await Course
            .find()
            .populate({ path: '_authors' })
            .or([
                { 'title': { $regex: regex } },
                { 'authors': { $elemMatch: { 'name': { $regex: regex } } } }
            ])
            .sort({ [sort_type]: sort_order })
            .skip((page - 1) * limit)
            .limit(limit)
            .exec();
        
        if (results) {
            return res.json({
                'page': page,
                'limit': limit,
                'total': total,
                'courses': results
            });
        };
    } catch (err) {
        return next(err);
    }
};

export const buyedCourses = async function (req, res, next) {
    const email = req.user.email;

    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    try {
        const user = await User.findOne({ email: email })
            .populate({
                path: 'courses.ref',
                populate: {
                    path: '_authors',
                    model: 'User'
                }
            })
            .exec();
        if (!user) return;

        const filtered = user.courses;

        if (!filtered.length) {
            return res.json([]);
        }
        
        const results = filtered.map(elem => elem.ref);
        return res.json(results);
    } catch (err) {
        console.log(err);
    };
};


export const add = async function (req, res, next) {
    const email = req.user.email;
    const { title, subtitle, description, category, price } = req.body;
 
    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    try { 
        const user = await User.findOne({ email: email })
        .exec();
        if (!user) return;

        let filename = `${hostUrl}/images/anonymous.png`;
        const avatar = req.files ? req.files.avatar : null;
        if(avatar) {
            const name = avatar.name.replace('.', Date.now() + '.');
            const fullpath = path.resolve('wwwroot', '../public/img/', './' + name);
    
            await avatar.mv(fullpath, function (err) {
                if (err) {
                    return next(err);
                }
            });
    
            if(name && name.length > 0) {
                filename = `${hostUrl}/images/${name}`;
            }
        }

        const no = await getNo('Course');

        let course = Course({
            no: no,
            title: title,
            subtitle: subtitle,
            authors: [{
                no: user.no,
                name: _.get(user, 'profile.name')
            }],
            _authors: [user._id],
            _comments: [],
            category: category,
            price: price,
            picture: filename,
            description: description
        });

        course = await course.save();
        await User.update({
            '_id': user._id
        }, {
            $push: {
                courses: {
                    ref: course._id,
                    no: course.no
                }
            }
        });
        const lectureNo = await getNo('Lecture');
        let lecture = Lecture({
            no: lectureNo,
            _course: course._id,
            header: []
        });
        lecture = await lecture.save();
        return res.json({
            courseNo: course.no,
            courseId: course._id,
            lecture: lecture._id,
            lectureNo: lecture.no
        }).end();
    } catch (err) { 
        return next(err);
    };
};

export const update = async function (req, res, next) {
    return res.json({
        'update': "+"
    });
};

export const remove = async function (req, res, next) {
    return res.json({
        'update': "+"
    });
};