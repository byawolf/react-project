import path from 'path';
import AES from 'crypto-js/aes';
import User from '../models/user';
import Cart from '../models/cart';
import generateToken from '../services/token-jwt';
import config, {hostUrl} from '../config';
import getNo from './getNo';

export const userinfo = async function (req, res, next) {
    const email = req.user.email;

    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    try {
        const user = await User.findOne({ email: email })
            .populate({ path: 'courses.ref' })
            .exec();
        return res.json(user);
    } catch (err) {
        console.log(err);
    }
};

export const signin = function (req, res, next) {
    res.send({token: generateToken(req.user)});
};

export const signout = async function (req, res, next) {
    try { 
        await req.session.destroy();
        req.logout();
        res.send("logout success.");
    } catch (err) {
        return res.status(400).send(err);
    };
};

export const signup = async function (req, res, next) {
    const { email, password, name, role, files } = req.body;

    if (!email || !password || !name) {
        return res.status(400).send({error: 'Invalid user information.'});
    }

    try {
        const exists = await User.findOne({ email: email });
        if (exists) {
            return res.status(409).send({error: 'Email is already in use'});
        }

     } catch (err) {
        return next(err);
    }
    
    const encrypted = AES.encrypt(password, config.secret).toString();
    if (!encrypted || encrypted.length <= 0) { 
        return res.status(400).send({error: 'invalid password.'});
    }
    
    let filename = `${hostUrl}/images/anonymous.png`;
    const avatar = req.files ? req.files.avatar : null;


    if(avatar) {
        const name = avatar.name.replace('.', Date.now() + '.');
        const fullpath = path.resolve('wwwroot', '../public/img/', './' + name);

        await avatar.mv(fullpath, function (err) {
            if (err) {
                return next(err);
            }
        });

        if(name && name.length > 0) {
            filename = `${hostUrl}/images/${name}`;
        }
    }
        
    const no = await getNo('User');
    const user = new User({
        email: email,
        no: no,
        password: encrypted,
        profile: {
            name: name,
            picture: filename,
            instructor: role === "instructor"
        }
    });

    try { 
        user.save();
 
        const cart = new Cart({
            user: user._id
        });
        cart.save();

        return res.json({token: generateToken(user)}).end();
    } catch (err) {
        return next(err);
    };
};
