import Cart from '../models/cart';
import Course from '../models/course';
import User from '../models/user';

const pullCart = async function (user, course) {
    try {
        const cart = await Cart
            .findOne({ user: user._id })
            .populate({ path: 'courses.ref' })
            .exec();
        
        if (!cart.courses.length) return;
      
        await Cart.update(
            {
                '_id': cart._id
            },
            {
                $pull: {
                    courses: {
                        ref: course._id,
                        no: course.no
                    }
                }
            }
        );
    } catch (err) {
        console.log(err);
    }
};

export const buyCart = async function (req, res, next) {
    const email = req.user.email;

    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    const course_no = req.body.course_no;

    if (!course_no || course_no.length < 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    let user, course;
    try { 
        course = await Course
            .findOne({ no: course_no })
            .populate({ path: '_authors' })
            .exec();
        
        user = await User.findOne({ email: email })
            .populate({ path: 'courses.ref' })
            .exec();
       
        const filtered = user.courses.filter( elem => elem.no === course.no);

        if (filtered.length <= 0) {
            await User.update({
                '_id': user._id
            }, {
                $push: {
                    courses: {
                        ref: course._id,
                        no: course.no,
                        learn: true
                    }
                }
            });
        } else {
            filtered[0].learn = !filtered[0].learn;
            await User.update(
                {
                    '_id': user._id
                },
                {
                    $set: {
                        courses: user.courses
                    }
                }
            );
        }

    } catch (err) {
        console.log(err);
     };

    if (user && course) {
        pullCart(user, course);
        return res.json({"learn": "success"});  
    }
};

export const listCart = async function (req, res, next) {
    const user = req.user;

    if (!user) {
        return res.status(400).send({error: 'invalid user.'});
    }

    try {
        const cart = await Cart
            .findOne({ user: user._id })
            .populate('courses.ref')
            .exec();
        
        const courses = cart.courses.map( elem => elem.ref );
        return res.json(courses);
    } catch (err) { 
        console.log(err);
    };
};

export const addCart = async function (req, res, next) {
    const user = req.user;

    if (!user) {
        return res.status(400).send({error: 'invalid user.'});
    }

    const course_no = req.body.course_no;

    if (!course_no || course_no.length < 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    try { 
        const course = await Course
            .findOne({ no: course_no })
            .populate({ path: '_authors' })
            .exec();
        
        const cart = await Cart
            .findOne({user: user._id})
            .populate({path: 'courses.ref'})
            .exec();
        const filtered = cart.courses.filter(elem => elem.no === course.no);

        if (filtered.length <= 0) {
            await Cart.update({
                '_id': cart._id
            }, {
                $push: {
                    courses: {
                        ref: course._id,
                        no: course.no
                    }
                }
            });
        }

        return res.json({
            "cart": "added"
        });
    } catch (err) {
        console.log(err);
        return next(err);
    };
};

export const removeCart = async function (req, res, next) {
    const user = req.user;

    if (!user) {
        return res.status(400).send({error: 'invalid user.'});
    }

    const course_no = req.body.course_no;

    if (!course_no || course_no.length < 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    try {
    const course = await Course
            .findOne({ no: course_no })
            .populate({ path: '_authors' })
            .exec();
        
    const cart = await Cart
            .findOne({ user: user._id })
            .populate({ path: 'courses.ref' })
            .exec();  
        const filtered = cart.courses.filter( elem => elem.no === course_no);
        if (filtered.length > 0) {
            await Cart.update({'_id': cart._id}, {
                $pull: {
                    courses: {
                        ref: course._id,
                        no: course.no
                    }
                }
            });
        }

        return res.json({"cart":"removed"});
                
     } catch (err) { 
        console.log(err);
     };
};
