import Cart from '../models/cart';
import Comment from '../models/comment';
import Lecture from '../models/lecture';
import Course from '../models/course';
import User from '../models/user';

const models = {
  Cart: Cart,
  Comment: Comment,
  Lecture: Lecture,
  Course: Course,
  User: User
}

export default async function (model) { 
  if (!model) return;

  const result = await models[model].findOne().sort({ field: 'asc', _id: -1 }).limit(1);

  if (result && result.no) { 
    return result.no + 1;
  };

  const length = await models[model].count({});
  return length + 1;
}