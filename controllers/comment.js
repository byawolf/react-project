import _ from 'lodash';
import Course from '../models/course';
import User from '../models/user';
import Comment from '../models/comment';
import getNo from './getNo';


export const paginate = async function (req, res, next) {
    const course_no = parseInt(req.query.course_no);
    if (!course_no || course_no <= 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    const page = parseInt(req.query.page);
    if (!page || page <= 0) {
        return res.status(400).send({error: 'invalid page.'});
    }

    const limit = parseInt(req.query.limit);
    if (!limit || limit <= 0) {
        return res.status(400).send({error: 'invalid limit.'});
    }

    try {
        const course = await Course
            .findOne({ no: course_no })
            .populate({ path: '_comments' })
            .exec();
        
        if (!course) return;
    
        const results = await Comment
            .find({ course: course_no })
            .populate({ path: '_user' })
            .sort({ 'no': -1 })
            .skip((page - 1) * limit)
            .limit(limit)
            .exec();
        
        
        return res.json({
            'page': page,
            'limit': limit,
            'total': Math.ceil(course._comments.length / limit),
            'comments': results
        });
    } catch (err) {
        console.log(err);
        return next(err);
    }
};

export const addComment = async function (req, res, next) {
    const email = req.user.email;

    if (!email || email.length < 0) {
        return res.status(400).send({error: 'invalid email.'});
    }

    const course_no = req.body.course_no;

    if (!course_no || course_no.length < 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    try {
        const course = await Course
            .findOne({ no: course_no })
            .populate({ path: '_comments' })
            .exec();
        if (!course) return;

        const user = await User.findOne({ email: email })
            .populate({ path: 'courses.ref' })
            .exec();
        if (!user) return;

        const content = req.body.content.replace(/(?![^<]*>)/g, "&nbsp;").replace(/\n\r?/g, "<br/>");
        const no = await getNo('Comment');
        const comment = Comment({
            no: no,
            course: course.no,
            data: Date.now,
            name: _.get(user, 'profile.name'),
            user: user.no,
            _user: user._id,
            content: content,
            rating: req.body.rating
        });

        await comment.save();
    
        const raw = Course
            .update(
                {
                    'no': course_no
                },
                {
                    $push: {
                        _comments: comment._id
                    }
                }
            );
        
        if (raw) {
            return res.json({"comment": "added"});
        }
    } catch (err) {
        console.log(err);
    }
};

export const removeComment = async function (req, res, next) {
    const user = req.user;
    if (!user) {
        return res.status(400).send({error: 'invalid user.'});
    }

    const course_no = req.body.course_no;
    if (!course_no || course_no <= 0) {
        return res.status(400).send({error: 'invalid course.'});
    }

    const comment_no = parseInt(req.body.comment_no);
    if (!comment_no || comment_no <= 0) {
        return res.status(400).send({error: 'invalid comment.'});
    }

           
            try {
                const result = await Comment
                .findOne({ course: course_no, no: comment_no })
                .exec();
               
                await Comment.findByIdAndRemove(result._id);
                return res.json({"comment": "deleted"});
            } catch (err) {
                console.log(err);
                return handleError(err);
            }
};
