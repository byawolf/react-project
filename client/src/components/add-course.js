import React, {Component} from 'react';
import PropTypes from 'prop-types';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import {addCourse} from '../actions/course';
import RichTextEditor from 'react-rte';
import CurriculumAdd from './curriculum-add.js'

import path from 'path';
import TextInput from '../components/text-input';

import Header from '../components/header';
import Footer from '../components/footer';


class AddCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            muiTheme: getMuiTheme(),
            dialogStyle: {display: 'none'},
            isSubmitting: false,
            subtitle: RichTextEditor.createEmptyValue(),
            description: RichTextEditor.createEmptyValue(),
            lectures: []
        };
    }

    addChapter = (e) => {
        e.preventDefault();
        const lectures  = [...this.state.lectures];
        if (lectures.length && !lectures[lectures.length - 1].title) { 
            return;
        }
        
        const defaultChapter = {
            title: '',
            body: []
        };
        lectures.push(defaultChapter);
        this.setState({
            lectures: lectures
        });
    };

    deleteChapter = (chapterIndex, e) =>{
        e.preventDefault();
        const lectures  = this.state.lectures;
        this.setState({
            lectures: [...lectures.slice(0, chapterIndex), ...lectures.slice(chapterIndex + 1)]
        });
     };

    addLecture = (chapterIndex, e) => {
        e.preventDefault();
        const lectures  = [ ...this.state.lectures ];
        if (lectures[chapterIndex].body.length && !lectures[chapterIndex].body[lectures[chapterIndex].body.length-1].content) { 
            return;
        }
        const defaultLecture = {
            content: "",
            video: ''
        };
        lectures[chapterIndex].body.push(Object.assign({}, defaultLecture) );
       
        this.setState({
            lectures: lectures
        });
    };

    deleteLecture = (chapterIndex, lectureIndex, e) => { 
        e.preventDefault();
        const lectures = [...this.state.lectures];
        const body = lectures[chapterIndex].body;
        lectures[chapterIndex].body = [...body.slice(0, lectureIndex), ...body.slice(lectureIndex + 1)];
        this.setState({
            lectures: lectures
        });
    };

    getTotalLectures = () => {
        return this.state.lectures.reduce((accumulator, currentValue) => { 
            return accumulator + currentValue.body.length;
        }, 0);
    };

    updateLectures = (index, event) => {
        const lectures = [...this.state.lectures];
        let lectureIndex;
        switch(event.target.name){
            case 'chapter-title':
                lectures[index].title = event.target.value;
                break;
            case 'lecture-title':
                lectureIndex = event.target.dataset.lectureIndex;
                lectures[index].body[lectureIndex].content = event.target.value;
                break;
            case 'file-to-upload':
                lectureIndex = event.target.dataset.lectureIndex;
                lectures[index].body[lectureIndex].video = event.target.files[0];
                break;
        }

        
        this.setState({
            lectures: lectures
        });
    };

    static childContextTypes = {
        muiTheme: PropTypes.object
    };

    getChildContext() {
        return {muiTheme: this.state.muiTheme};
    }

    static propTypes = {
        topOffset: PropTypes.number,
        leftOffset: PropTypes.number
    };

    componentDidMount() {
        this.setState({
            dialogStyle: {
                display: 'flex',
                justifyContent: 'center',
                marginTop: 0,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0,
                top: this.props.topOffset,
                left: this.props.leftOffset
            }
        });
    }

    
    submitForm = (e) => {
        e.preventDefault();

        const {isSubmitting} = this.state;

        if(isSubmitting) return;

        const title = $('#title').val();
        const subtitle = this.state.subtitle.toString('html');
        const description = this.state.description.toString('html');
        const category = $('#category').val();
        const price = $('#price').val();
        const fileToUpload = document.getElementById('fileToUpload').files;

        if(!isSubmitting) {
            $('#submit').html('<img src="/public/assets/images/spinner.gif"/>');
            this.setState({isSubmitting:true});
        }

        const failed = () => {
            $('#submit').html('Submit');
                this.setState({isSubmitting:false});
        };
        
        let avatar = null;

        if (fileToUpload) {
            if (fileToUpload[0]) {
                const filename = fileToUpload[0].name;

                if (filename && filename.length > 0) {
                    const ext = path.extname(filename).toLowerCase();

                    if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif' || ext === '.ico') {
                        avatar = fileToUpload[0];
                    }
                }
            }
        }


        this.props.addCourse({title, subtitle, description, category, price, avatar, lectures: this.state.lectures, failed});
    };

    sereverError = () => {
        if (this.props.hasError) {
            return (
                <div className="alert alert-danger">
                    <strong>There was a loading error</strong>
                </div>
            );
        }
    };

    uploadFile(event) {
        const file = event.target.files[0];
        if(file) {
            const ext = path.extname(file.name).toLowerCase();

            if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif' || ext === '.ico') {
                let reader = new FileReader();

                reader.onload = function (e) {
                    const avatar_image = $('#avatar_image');
                    avatar_image.attr('src', e.target.result);
                    const avatar_text = $('#avatar_text');
                    avatar_text.attr('placeholder', file.name);
                    avatar_image.show();
                };

                reader.readAsDataURL(file);
            }
        }
        else {
            $('#avatar_image').hide();
        }
    }

    renderButton = () => {
        return (
            <div style={{display: 'flex', justifyContent:'left', alignItems:'center', width:'100%', height:'60px'}}>
                <RaisedButton
                    label="Course Image"
                    labelPosition="before"
                    style={{marginLeft: 0, marginRight:20}}
                    containerElement="label"
                >
                    <input
                        type="file"
                        style={{
                            cursor: 'pointer',
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0,
                            width: '100%',
                            opacity: 0,
                            marginRight: 20
                        }}
                        id="fileToUpload"
                        name="fileToUpload"
                        className="form-control"
                        onChange={this.uploadFile}/>
                </RaisedButton>
                <div>
                    <input id="avatar_text" type="text" readOnly="true" className="form-control" placeholder=""/>
                </div>
                <div>
                    <img id="avatar_image" width="50px" height="50px" alt="" className="img-circle" style={{marginLeft:4,marginRight:4, verticalAlign:'middle'}}/>
                </div>
            </div>
        );
    };

    validateTitle = (value) => {
        let error = '';
        if (!value || value.length <= 0) {
            error = 'Required';
        }

        return error;
    };

    validateSubtitle = (value) => {
        let error = '';
        if (!value || value.length <= 0) {
            error = 'Required';
        }

        return error;
    };

    validateDescription = (value) => {
        let error = '';
        if (!value || value.length <= 0) {
            error = 'Required';
        }

        return error;
    };

    validateCategory = (value) => {
        let error = '';
        if (!value || value.length <= 0) {
            error = 'Required';
        }

        return error;
    };

    validatePrice = (value) => {
        let error = '';
        if (!value || value.length <= 0 || value < 1) {
            error = 'Min >= 1';
        }

        return error;
    };

    changeSubtitle = (value) => {
        this.setState({ subtitle: value });
    };

    changeDescription = (value) => {
        this.setState({ description: value })
    }


    reset = () => {
        this._title.reset();
        this._subtitle.reset();
        this._description.reset();
        this._category.reset();
        this._price.reset();
        

        $('#fileToUpload').val('');
        $('#avatar_image').attr('src', '').hide();
        $('#avatar_text').attr('placeholder', '');
    };

    render() {
        return (
            <div>
                <Header/>
                <div style={{textAlign: 'center'}}>
                    {this.sereverError()}
                </div>
                <div style={this.state.dialogStyle}>
                    <form style={{width:'60%', height:'100%',
                    marginTop:20, marginBottom:100}}
                        id='form_info'
                        name='form_info'
                        onSubmit={(e) => this.submitForm(e)}>
                        <TextInput
                            ref={e=>this._title = e}
                            label="Title"
                            name="title"
                            type="text"
                            validate={this.validateTitle}
                        />
                         <label htmlFor="subtitle" className="control-label">Subtitle</label>
                        <RichTextEditor id="subtitle" value={this.state.subtitle}  onChange={this.changeSubtitle}/>
                        <label htmlFor="description" className="control-label">Description</label>
                        <RichTextEditor id="description" value={this.state.description} onChange={this.changeDescription}/>
                         <TextInput
                            ref={e=>this._category = e}
                            label="Category"
                            name="category"
                            type="text"
                            validate={this.validateCategory}
                        />
                         <TextInput
                            ref={e=>this._price = e}
                            label="Price"
                            name="price"
                            type="number"
                            validate={this.validatePrice}
                        />
                      
                        <CurriculumAdd updateLectures={this.updateLectures} getTotalLectures={this.getTotalLectures} addLecture={this.addLecture} addChapter={this.addChapter} deleteChapter={this.deleteChapter} deleteLecture={this.deleteLecture} lectures={this.state.lectures}/>
                        <div className="form-group">
                            {this.renderButton()}
                        </div>
                        <div style={{clear:'both'}}>&nbsp;</div>
                        <div style={{display:'flex', justifyContent:'center'}}>
                            <button
                                id="submit"
                                type="submit"
                                value="Submit"
                                name="submit"
                                className="btn btn-lg btn-primary">Submit</button>
                            &nbsp;&nbsp;&nbsp;
                            <button
                                type="button"
                                value="Clear"
                                name="clear"
                                className="btn btn-lg btn-default"
                                onClick={(e) => {
                                    e.preventDefault();
                                    this.reset();
                                }}>Clear</button>
                        </div>
                    </form>
                </div>
            <Footer/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        hasError: state.addCourseFailure,
        isLoading: state.fetchCourseLoading
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addCourse: ({title, subtitle, description, category, price, avatar, lectures, failed}) => dispatch(addCourse({title, subtitle, description, category, price, avatar, lectures, failed}))
    }
};

export default AddCourse = withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCourse));
