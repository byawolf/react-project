import _ from 'lodash';
import React, {Component} from 'react';
import Readmore from 'react-viewmore';
import PropTypes from 'prop-types';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Dialog from 'material-ui/Dialog';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchLecture, viewLecture} from '../actions/lecture';
import { userInfo } from '../actions';
import TextInput from './text-input';
import RaisedButton from 'material-ui/RaisedButton';

import SignIn from '../auth/signin';

import '../../styles/detail.css';

const styles = {
    dialogRoot: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 0
    },
    dialogContent: {
        position: "relative",
        width: "80vw",
        transform: "",
    },
    dialogBody: {
        paddingBottom: 0
    }
};

const CustomComponent = ({ children, ...props }) => (
    <div {...props} style={{
        color: 'white',
        backgroundColor: 'pink',
        padding: '5px 10px',
        textAlign: 'center'
    }}>
        {children}
    </div>
);


class Curriculum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            muiTheme: getMuiTheme(),
            open: false
        };
    }

    uploadFile(event) {
        const lectureIndex = event.target.dataset.lectureIndex;
        const file = event.target.files[0];
        const video_text = $(`#video_text_${lectureIndex}`);
        video_text.attr('placeholder', file.name);
    }

    static childContextTypes = {
        muiTheme: PropTypes.object
    };

    getChildContext() {
        return {muiTheme: this.state.muiTheme};
    }

    collapsibleTree = () => {
        const icons = {
            plus: 'add_box',
            minus: 'indeterminate_check_box'
        };

        $('.collapsible').collapsible({
            accordion: false,
            onOpen: function(el) {
                if($(el).hasClass('active')) {
                    const collapsible = el.children('.collapsible-header');
                    if (collapsible) {
                        const icon = $(collapsible).children('.material-icons');
                        icon.text(icons.minus);
                    }
                }
            },
            onClose: function(el) {
                if (!$(el).hasClass('active')) {
                    const collapsible = el.children('.collapsible-header');
                    if (collapsible) {
                        const icon = $(collapsible).children('.material-icons');
                        icon.text(icons.plus);
                    }
                }
            }
        });
    };

    componentDidMount() {
        const token = localStorage.getItem('token');
        if(token) {
            this.props.userInfo();
        }

        this.collapsibleTree();
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleViewLecture = (event, header, body, learn) => {
        console.log('hn:'+header.no+':bn:'+body.sub_no+':content:'+body.content);
        event.preventDefault();

        if(learn) {
            if (this.props.logged) {
                const {user, lecture} = this.props;

                if (!lecture) {
                    return this.handleOpen();
                }

                if (user) {
                    return this.props.viewLecture(lecture.no, header.no, body.sub_no);
                }
            }
        }
        else {
            this.handleOpen();
        }
    };

    renderDialog = () => {
        return (
            <div>
                <Dialog
                    contentStyle={ styles.dialogContent }
                    bodyStyle={ styles.dialogBody }
                    style={ styles.dialogRoot }
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    repositionOnUpdate={false}
                    autoScrollBodyContent={true}
                >
                    <SignIn redirect={this.handleClose} dialog={true}/>
                    <br/>
                    <br/>
                </Dialog>
            </div>
        );
    };
    
    valueRequired = (value) => {
        let error = '';
        if (!value || value.length <= 0) {
            error = 'Required';
        }
        return error;
    };


    lectureBody = (lectures, chapterIndex) => {
        return lectures.map( (lecture, i) => {

            return (
                
                    <div className="row text-size-fifth flex lecture-item" key={i}>
                    <div className="col-sm-6 text-left">
                    <button type="button" className="btn btn-danger" onClick={this.props.deleteLecture.bind(this, chapterIndex, i)}>
                    <i className="material-icons collapsible-icon">delete_forever</i>
                </button>   
                        <TextInput
                            required
                            label="Title"
                            value={lecture.conten}
                            name="lecture-title"
                            type="text"
                            className="lecture-label"
                            data-lecture-index={i}
                        />
                        </div>
                        <div className="col-sm-6 text-right mb-12">
                            {this.renderVideoButton(i)}
                        </div>
                    </div>
              
            )}
        );
    };

    renderVideoButton = (lectureIndex) => {
        return (
            <div style={{display: 'flex', justifyContent:'left', alignItems:'center', width:'100%', height:'100%'}}>
                <RaisedButton
                    className="lecture-label"
                    label="Video"
                    labelPosition="before"
                    style={{marginLeft: 0, marginRight:20}}
                    containerElement="label"
                >
                    <input
                        type="file"
                        style={{
                            cursor: 'pointer',
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0,
                            width: '100%',
                            opacity: 0,
                            marginRight: 20
                        }}
                        data-lecture-index={lectureIndex}
                        id="fileToUpload"
                        name="file-to-upload"
                        className="form-control"
                        onChange={this.uploadFile}/>
                </RaisedButton>
                <div>
                    <input id={`video_text_${lectureIndex}`} type="text" readOnly="true" className="form-control" placeholder=""/>
                </div>
            </div> 
        );
    };

    lectureChapter = () => {
        return this.props.lectures.map((chapter, i) => {
            return (
                <li key={i} style={{ position: 'reletive' }}  onChange={this.props.updateLectures.bind(this, i)} className="chapter-item">
                    <div className="collapsible-header" style={{borderBottom: 'none', position: 'absolute', background: 'transparent'}}><i
                        className="material-icons collapsible-icon">add_box</i></div>
                        
                    <TextInput
                    validate={this.valueRequired}
                        label="Title"
                        value={chapter.title}
                        name="chapter-title"
                        type="text"
                        className="col-sm-6"
                    />
                     <button type="button" className="btn btn-danger"  onClick={this.props.deleteChapter.bind(this, i)}>
                    <i className="material-icons collapsible-icon">delete_forever</i>
                </button>   
                    <div className="collapsible-body update-lectures">
                    
                    {this.lectureBody(chapter.body, i)}
                    <button
                            onClick={this.props.addLecture.bind(this, i)}
                            id="addlecture"
                            className="btn">Add Lecture</button>
                    </div>
                </li>
            )}
        );
    };

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            const event = document.createEvent('HTMLEvents');
            event.initEvent('resize', true, false);
            window.dispatchEvent(event);
        }
    }

    renderLecture = () => {
        return (
            <div className="curriculum-add">
            <div className="container" style={{
                textAlign:'center',
                width: '100%'
            }}>
                <div className="row" style={{
                    backgroundColor:'rgba(250,250,250,0.8)',
                    verticalAlign:'middle',
                    lineHeight:'4rem',
                    display:'flex',
                    justifyContent:'space-between'
                }}>
                    <div className="col-sm-7 text-left">
                        <span className="text-bold text-size-second" style={{marginLeft:'2rem'}}>
                        Curriculum
                        </span>
                    </div>
                    <div className="col-sm-3 text-center">
                        <span className="text-emphasis-fifth text-size-third">
                           {this.props.getTotalLectures()}
                        </span>
                        <span className="text-size-third">
                        &nbsp;Lectures
                        </span>
                    </div>
                </div>
            </div>
                <Readmore id='showmore'
                          type={CustomComponent}>
                    <div className="container-fluid" style={{
                        textAlign: 'center',
                        width: '100%'
                    }}>
                        <div className="row">
                            <div className="col-sm-12">
                                <ul className="collapsible collapsible-scroll" data-collapsible="expandable">
                                    {this.lectureChapter()}
                                </ul>
                            </div>
                        </div>
                        <button
                            onClick={this.props.addChapter}
                            id="addChapter"
                            className="btn" style={{ margin: '10px' }}>Add chapter</button>
                    </div>
                </Readmore>
            </div>
        );
    };

    render() {
        return (
            <div className="curriculum-add">
                {this.renderDialog()}
                {this.renderLecture()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        logged: state.auth.logged,
        user: state.auth.user,
        lecture: state.lecture.lecture,
        hasError: state.lecture.error,
        isLoading: state.lecture.loading
    };
}

const mapDispatchToProps = dispatch => {
    return {
        fetchLecture: (course_no) => dispatch(fetchLecture(course_no)),
        userInfo: () => dispatch(userInfo()),
        viewLecture: (lecture_no, header_no, sub_no) => dispatch(viewLecture(lecture_no, header_no, sub_no))
    }
};

export default Curriculum = withRouter(connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(Curriculum));
