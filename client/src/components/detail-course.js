import _ from 'lodash';
import dateFormat from 'dateformat';
import {hostUrl} from '../../config';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchDetailCourse} from '../actions/course';
import centerComponent from 'react-center-component';
import CircularProgress from 'material-ui/CircularProgress';

import ShowMore from 'react-show-more';

import Header from './header';
import Footer from './footer';
import CartBanner from './cart-banner';
import Curriculum from './curriculum';
import Comment from './comment';

import '../../styles/detail.css';

const numberWithCommas = (x) => {
    let parts = parseInt(x).toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};

@centerComponent
class DetailCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            muiTheme: getMuiTheme(),
            dialogStyle: {display: 'none'}
        };
    }

    static childContextTypes = {
        muiTheme: PropTypes.object
    };

    getChildContext() {
        return {muiTheme: this.state.muiTheme};
    }

    static propTypes = {
        topOffset: PropTypes.number,
        leftOffset: PropTypes.number
    };

    componentDidMount() {
        this.setState({
            dialogStyle: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 100,
                marginBottom: 100,
                width: '100%',
                height: '100%',
                top: this.props.topOffset,
                left: this.props.leftOffset
            }
        });

        window.scrollTo(0, 0);
        this.props.fetchDetailCourse(this.props.match.params.id);
    }

    renderState = () => {
        if (this.props.hasError) {
            return (
                <div className="alert alert-danger">
                    <div style={{textAlign: 'center'}}>
                        <strong>There was a loading error</strong>
                    </div>
                </div>
            );
        }

        if (this.props.isLoading) {
            return (
                <div style={this.state.dialogStyle}>
                    <CircularProgress size={60} thickness={7}/>
                </div>);
        }
    };

    authorNames = (authors) => {
        return _.map(authors, (author, i) => {
            let decorator = ', ';
            if(i === authors.length-1) {
                decorator = '';
            }

            return (
                <span key={i}>
                    <a className="text-emphasis-first">{author.profile.name}</a><a className="text-white">{decorator}</a>
                </span>
            );
        });
    };


    renderCourse = () => {
        const {course} = this.props;

        if (!course) return (<div>&nbsp;</div>);

        return (
            <div className="container-fluid">
                <div className="row header-top">
                    <div className="col-sm-12">
                        <div className="container">
                            <div className="row body-content">
                                <div className="col-sm-8">
                                    <br/>
                                    <br/>
                                    <div className="text-white text-size-first">{course.title}</div>
                                    <br/>
                                    <div className="text-white text-size-second" dangerouslySetInnerHTML={{ __html: course.subtitle }}></div>
                                    

                                    <div className="text-white text-size-third text">Created
                                        by {this.authorNames(course._authors)}</div>
                                    <div className="text-white text-size-third text">Last updated <span
                                        className="text-emphasis-third">{dateFormat(course.updated, "m/yyyy")}</span>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                        <div style={{marginTop:30}} className="xs-hide">
                                        </div>
                                    <br/>
                                    <CartBanner course={course}/>
                                </div>
                            </div>
                            <div className="row body-content hidden-xs">
                                <div className="col-sm-12">
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                </div>
                <div className="container">
                    <div className="row body-content">
                        <div className="col-sm-offset-1 col-sm-10">
                            <br/>
                            <br/>
                            <div className="text-size-second text-bold">Description</div>
                            <br/>
                            <ShowMore
                                lines={5}
                                more='Show more'
                                less='Show less'
                                anchorClass=''
                            >
                                <div dangerouslySetInnerHTML={{ __html: course.description }}/>
                            </ShowMore>
                            <br/>
                            <br/>
                            <Curriculum/>
                        </div>
                    </div>
                </div>
               
               
                <br/>
                <div className="container">
                    <Comment/>
                </div>
            </div>
        );
    };

    render() {
        return (
            <div className="detail-course">
                <Header {...this.props}/>
                <div>
                    {this.renderState()}
                </div>
                <div style={{
                    marginTop: 20,
                    marginBottom: 20
                }}>
                    {this.renderCourse()}
                </div>
                <Footer {...this.props}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        course: state.fetchDetailCourseDone,
        hasError: state.fetchCourseFailure,
        isLoading: state.fetchCourseLoading
    };
}

const mapDispatchToProps = dispatch => {
    return {
        fetchDetailCourse: (course_no) => dispatch(fetchDetailCourse(course_no))
    }
};

export default DetailCourse = withRouter(connect(mapStateToProps, mapDispatchToProps)(DetailCourse));
