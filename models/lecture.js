import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const LectureSchema = new Schema({
    no: {type: Number},
    _course: {type: ObjectId, ref: 'Course'},
    header: [{
        no: {type: Number, default: 0},
        title: {type: String, default: ''},
        body: [{
            sub_no: {type: Number, default: 0},
            content: {type: String, default: ''},
            preview: {type: Boolean, default: false},
            video: {type: String, default: ''}
        }]
    }]
});

export default mongoose.model('Lecture', LectureSchema, 'lectures');
