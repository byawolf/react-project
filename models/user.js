import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const UserSchema = new Schema({
    no: Number,
    email: {type: String, unique: true, lowercase: true},
    password: { type: String, default: '' },
    profile: {
        name: {type: String, default: ''},
        picture: {type: String, default: ''},
        instructor: {type: Boolean, default: false}
    },
    courses: [{
        ref: { type: ObjectId, ref: 'Course'},
        no: { type: Number, default: 0},
        learn: { type: Boolean, default: false}
    }]
});

export default mongoose.model('User', UserSchema, 'users');
