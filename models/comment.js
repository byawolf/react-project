import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const CommentSchema = new Schema({
    no: Number,
    course: {type: Number, default: 0},
    date: {type: Date, default: Date.now},
    name: {type: String, default: ''},
    _user: {type: ObjectId, ref: 'User'},
    content: {type: String, default: ''},
    rating: {type: Number, default: 0},
    indent: {type: Number, default: 0},
    origin: {type: String, default: ''},
    _reply: {type: ObjectId, ref: 'Comment'}
});

export default mongoose.model('Comment', CommentSchema, 'comments');
